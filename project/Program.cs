﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace project
{
    class Program
    {
        //je potreba zadat maximalni delku slova + 1
        const int max_characters = 9;
        static char[] field = new char[max_characters];
        static int maxLevelNow = 0;
        static string password;
        static string hash;
        static int low;
        static int high;
        static int procentaPocet;
        static int procentaSuma;
        static int procentaPom;
        static int procentaDesitky = 20;
        static string currentPasswd;
        static string pomHash;
        static MD5 md5Hash;

        const int vlaken = 5;
        static MD5[] md5Hash_paralel = new MD5[vlaken];
        static string[] pomHash_paralel = new string[vlaken];
        static string[] currentPasswd_paralel = new string[vlaken];
        static char[][] field_paralel = new char[vlaken+1][];
        private static Object thisLock;
        private static bool nalezeno = false;
        static Thread[] t = new Thread[vlaken];

        
        //length = delka slova, ktere generuji
        private static bool alg_old(int level = 0)
        {
            while (true)
            {
                for (int x = low; x < high; x++)
                {
                    if (level == max_characters)
                    {
                        return false;
                    }
                    field[level] = (char)x;
                    if (level != 0)
                    {
                        if (alg_old(level - 1))
                        {
                            return true;
                        }
                    }
                    /*
                    //zde je algorytmus hloupy schvalne, nebot tim simuluje, ze delku slova nezna...
                    found = true;
                    for (int i = 0; i < password.Length; i++)
                    {
                        if (password.Length != maxLevelNow || password[i] != field[i])
                        {
                            found = false;
                            break;
                        }
                    }
                    */
                    //uprava na MD5
                    currentPasswd = new string(field, 0, maxLevelNow+1);
                    pomHash = GetMd5Hash(md5Hash, currentPasswd);
                    if (hash.Equals(pomHash))
                    {
                        //konec
                        System.Console.WriteLine("SHODA: ");
                        System.Console.WriteLine("");
                        System.Console.WriteLine("HASH: " + pomHash);
                        System.Console.Write("HESLO: ");
                        for (int i = 0; i <= maxLevelNow; i++)
                        {
                            Console.Write(field[i]);
                        }
                        System.Console.WriteLine("");
                        System.Console.WriteLine("");
                        return true;
                    }
                    else
                    {
                        /*
                        //vypisy vsech slov
                        for (int i = 0; i <= maxLevelNow; i++)
                        {
                            Console.Write(field[i]);
                        }
                        System.Console.WriteLine("");
                        */

                        if (level == maxLevelNow)
                        {
                            //posledni slovo aktualni delky
                            /*
                            for (int i = 0; i <= maxLevelNow; i++)
                            {
                                Console.Write(field[i]);
                            }
                            System.Console.WriteLine("");
                            */
                            procentaPom = (100/procentaSuma) * procentaPocet;
                            if(procentaPom > procentaDesitky){
                                System.Console.WriteLine("" + procentaPom + "%");
                                //10-maxLevelNow proto, aby vypisy probihaly casteji...
                                procentaDesitky += (20 - 2*(maxLevelNow+1));
                            }
                            procentaPocet++;

                            //!!!!!!!!!!!!!!!!
                            //je potreba projet jeste jednou kvuli tomu, ze kdyz se to prjizdelo tak jen pro mensi velikosti
                            if (level != 0 && x == low)
                            {
                                if (alg_old(level - 1))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                if (level == maxLevelNow)
                {
                    Console.WriteLine("Porovnána všechna slova s délkou " + (level + 1));
                    maxLevelNow = ++level;
                    //nulovani procent
                    procentaPocet = 0;
                    procentaDesitky = (20 - 2*(maxLevelNow+1));
                }
                else
                {
                    return false;
                }
            }
        }

        private static bool alg_paralel(int level = 0, int thread = 0)
        {
            if (thread > 0)
            {
                md5Hash = MD5.Create();
            }
            for (int x = low; x < high; x++)
            {
                if (nalezeno)
                {
                    return true;
                }

                if (level == max_characters)
                {
                    //prekrocena maximalni velikost zapisovaneho pole - pravdepodobne nenastane
                    return false;
                }
                field_paralel[thread][level] = (char)x;
                if (level > 1 && thread > 0)
                {
                    //System.Console.WriteLine("VLAKNO: " + thread + ": " + (char)x);
                }
                if (level != 0)
                {
                    // && level == maxLevelNow && x%((int)(high - low) / 4) == 0
                    //magic constant 12
                    int magic  = (int)(high - low) / level;

                    //jen hlavni vlakno muze volat dalsi vlakna, aby jich nebylo prilis mnoho...
                    if (level > 2 && x % magic == 0 && thread == 0)
                    {
                        //pokud to ma cenu - pokud zbyva jeste dost iteraci
                        if (x + magic / 2 < high)
                        {
                            if (level == 4)
                            {
                                Console.WriteLine(4);
                            }
                            for (int v = 0; v < vlaken-1; v++)
                            {
                                int pom = x;
                                if (t[v] != null)
                                {
                                    continue;
                                }
                                t[v] = new Thread(delegate()
                                {
                                    int vlakno = v;
                                    for (int i = 0; i < magic / 2; i++)
                                    {
                                        //System.Console.WriteLine("DALSI iterace VLAKNA: " + (char)pom);
                                        field_paralel[thread + 1 + v][level] = (char)pom;
                                        //pokud nejsou v poli znaky, prebiraji se z hlavniho vlakna 0...
                                        if (level < maxLevelNow)
                                        {
                                            for (int k = level + 1; k <= maxLevelNow; k++)
                                            {
                                                field_paralel[thread + 1 + v][k] = field_paralel[thread][k];
                                            }
                                        }
                                        alg_paralel(level - 1, thread + 1 + v);
                                        pom++;
                                        if (nalezeno)
                                        {
                                            break;
                                        }
                                    }
                                    t[vlakno] = null;
                                });
                                t[v].Start();
                                x += magic / 2;
                                break;
                            }
                        }

                        else
                        {
                            for (int v = 0; v < vlaken-1; v++)
                            {
                                if (t[v] != null)
                                {
                                    continue;
                                }
                                field_paralel[thread + 1 + v][level] = (char)x;
                                //pokud nejsou v poli znaky, prebiraji se z volajiciho vlakna...
                                if (level < maxLevelNow)
                                {
                                    for (int k = level + 1; k <= maxLevelNow; k++)
                                    {
                                        field_paralel[thread + 1 + v][k] = field_paralel[thread][k];
                                    }
                                }
                                t[v] = new Thread(delegate()
                                {
                                    int vlakno = v; 
                                    alg_paralel(level - 1, thread + 1 + v);
                                    t[vlakno] = null;
                                });
                                t[v].Start();
                                x++;
                                break;
                            }
                        }
                        field_paralel[thread][level] = (char)x;
                        
                    }
                    else
                    {
                        if (alg_paralel(level - 1, thread))
                        {
                            return true;
                        }
                    }
                }
                if (level == 0)
                {
                    //System.Console.WriteLine("0: " + (char)x);
                }
                
                
                //uprava na MD5
                currentPasswd_paralel[thread] = new string(field_paralel[thread], 0, maxLevelNow + 1);
                lock (thisLock)
                {
                    pomHash_paralel[thread] = GetMd5Hash(md5Hash_paralel[thread], currentPasswd_paralel[thread]);
                }
                if (hash.Equals(pomHash_paralel[thread]))
                {
                    //konec
                    System.Console.WriteLine("SHODA: ");
                    System.Console.WriteLine("");
                    System.Console.WriteLine("HASH: " + pomHash_paralel[thread]);
                    System.Console.Write("HESLO: ");
                    for (int i = 0; i <= maxLevelNow; i++)
                    {
                        Console.Write(field_paralel[thread][i]);
                    }
                    System.Console.WriteLine("");
                    System.Console.WriteLine("");
                    nalezeno = true;
                    for (int v = 0; v < vlaken-1; v++)
                    {
                        if (thread - 1 == v)
                        {
                            continue;
                        }
                        if (t[v] != null)
                        {
                            try
                            {
                                t[v].Suspend();
                            }
                            catch (Exception e) { }
                        }
                    }
                    return true;
                }
                else
                {
                    /*
                    if (maxLevelNow > 3 && thread > 0)
                    {
                        
                        //vypisy vsech slov
                        lock (thisLock)
                        {
                            for (int i = 0; i <= maxLevelNow; i++)
                            {
                                Console.Write(field_paralel[thread][i]);
                            }
                            System.Console.WriteLine(" - vlákno: " + thread);

                            Console.Write("     0: → ");
                            for (int i = 0; i <= maxLevelNow; i++)
                            {
                                Console.Write(field_paralel[0][i]);
                            }
                            Console.Write(" → ");
                        }
                        
                         
                    }
                    */
                    if (level == maxLevelNow)
                    {
                        //posledni slovo aktualni delky
                        /*
                        for (int i = 0; i <= maxLevelNow; i++)
                        {
                            Console.Write(field_paralel[thread][i]);
                        }
                        System.Console.WriteLine("");
                        */
                        procentaPom = (100 / procentaSuma) * procentaPocet;
                        if (procentaPom > procentaDesitky)
                        {
                            //System.Console.Write(" - vlákno: " + thread);
                            System.Console.WriteLine(" → " + procentaPom + "%");
                            //10-maxLevelNow proto, aby vypisy probihaly casteji...
                            procentaDesitky += (20 - 2 * (maxLevelNow + 1));
                        }
                        procentaPocet++;

                        //!!!!!!!!!!!!!!!!
                        //je potreba projet jeste jednou kvuli tomu, ze kdyz se to prjizdelo tak jen pro mensi velikosti
                        if (level != 0 && x == low)
                        {
                            if (alg_paralel(level - 1, thread))
                            {
                                return true;
                            }
                        }
                    }
                }

                //cekani na vlakno
                if (level == maxLevelNow && x == high - 1 && t != null)
                {
                    for (int v = 0; v < vlaken-1; v++)
                    {
                        //aby necekalo vlakno samo na sebe
                        if (thread - 1 == v)
                        {
                            continue;
                        }
                        if (t[v] != null)
                        {
                            t[v].Join();
                        }
                    }
                }
            }
            if (level == maxLevelNow)
            {
                Console.WriteLine("Porovnána všechna slova s délkou " + (level + 1));
                maxLevelNow = ++level;
                //nulovani procent
                procentaPocet = 0;
                procentaDesitky = (20 - 2 * (maxLevelNow + 1));
            }
            return false;
            
        }
        //kod z MSDN
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        //TODO - zkusit to rozhodit do vlaken
        static void Main(string[] args)
        {

            System.Console.WriteLine("Set password: ");
            password = Console.ReadLine();
            //System.Console.WriteLine(password);
            /*
            Console.WriteLine("ASCII hodnoty pro zadane heslo: " + password + " → ");
            for (int i = 0; i < password.Length; i++)
            {
                Console.WriteLine("                                       " + (int)password[i]);
            }
            */
            md5Hash = MD5.Create();
            thisLock = new Object();
            hash = GetMd5Hash(md5Hash, password);
            Console.WriteLine("The MD5 hash pro zadane heslo: " + password + " → " + hash);
            Console.WriteLine("--------------------------------");

            //32 = SPACE, 48 = 0; 65 = A; 97 = a; 126 = ~
            /*
            low = 32;
            high = 127;
            */

            //vsechny znaky z klasicke ASCII
            /*
                low = 32;
                high = 256;
            */

            //jen mala pismena
            
            low = 97;
            high = 122 + 1;
            
            procentaSuma = high-1 - low;

            Thread.CurrentThread.Name = "0";

            for (int i = 0; i < vlaken; i++)
            {
                md5Hash_paralel[i] = MD5.Create();
                field_paralel[i] = new char[max_characters];
            }

            DateTime begin = DateTime.UtcNow;
            alg_old();
            //alg_paralel(6);
            DateTime end = DateTime.UtcNow;



            //inicializace
            maxLevelNow = 0;
            //nulovani procent
            procentaPocet = 0;
            procentaDesitky = (20 - 2 * (maxLevelNow + 1));

            DateTime begin2 = DateTime.UtcNow;
            //alg_old();
            alg_paralel(6);
            DateTime end2 = DateTime.UtcNow;



            //prepocet pro vystup
            int hour = (int)(end - begin).TotalHours;
            int minutes = (int)(end - begin).TotalMinutes - hour * 60;
            int seconds = (int)(end - begin).TotalSeconds - ((int)(end - begin).TotalMinutes) * 60;
            double miliseconds = (end - begin).TotalMilliseconds - ((int)(end - begin).TotalSeconds) * 1000;


            Console.WriteLine("Počáteční čas: " + begin);
            Console.WriteLine("Koncový čas: " + end);
            Console.WriteLine("Naměřený čas: " + hour + "h, " + minutes + "m, " + seconds + "s, " + miliseconds + " ms.");
            Console.WriteLine("");


            //prepocet pro vystup
            hour = (int)(end2 - begin2).TotalHours;
            minutes = (int)(end2 - begin2).TotalMinutes - hour * 60;
            seconds = (int)(end2 - begin2).TotalSeconds - ((int)(end2 - begin2).TotalMinutes) * 60;
            miliseconds = (end2 - begin2).TotalMilliseconds - ((int)(end2 - begin2).TotalSeconds) * 1000;


            Console.WriteLine("PARALEL");
            Console.WriteLine("Počáteční čas: " + begin2);
            Console.WriteLine("Koncový čas: " + end2);
            Console.WriteLine("Naměřený čas: " + hour + "h, " + minutes + "m, " + seconds + "s, " + miliseconds + " ms.");
            Console.WriteLine("");

            Console.WriteLine("Stiskněte klávesu pro zavření");
            Console.ReadKey();

            return;
        }

    }
}
